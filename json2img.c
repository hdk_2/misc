#include <stdio.h>
#include <err.h>

int
main ()
{
  int c, s = 0, d = 0, b[128], o = 0;
  unsigned v;

  while ((c = getc (stdin)) > 0)
    {
      if (c == '[')
	{
	  s = o = 0;
	  continue;
	}
      if (c == 's')
	s = d = 1;
      if (s || c == '{' || c == '}' || c == '\n' || c == ',')
	continue;
      if (c == ']')
	{
	  if (d)
	    {
	      while (o < 128)
		b[o++] = 0;
	      if (fwrite (b, sizeof b, 1, stdout) != 1)
		errx (1, "write error");
	      d = 0;
	    }
	  continue;
	}
      if (c != '-')
	ungetc (c, stdin);
      if (scanf ("%u", &v) != 1)
	errx (1, "data error 1 '%c'", c);
      if (o >= 128)
	errx (1, "data error 2");
      b[o++] = c == '-' ? -v : v;
    }
}
