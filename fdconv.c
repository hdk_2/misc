#include <stdio.h>
#include <err.h>

char buf[512 * 36];

int
main ()
{
  size_t len;

  while ((len = fread (buf, 1, 512 * 9, stdin)) == 512 * 9)
    if (fwrite (buf, 1, sizeof buf, stdout) != sizeof buf)
      errx (1, "fwrite error");
  if (len > 0)
    if (fwrite (buf, 1, sizeof buf, stdout) != sizeof buf)
      errx (1, "fwrite error");
}
